'''
author: Dr. Vladimir Molchanov
date: 27.05.2016
'''

import math
import random
import traceback
import struct
import os
import os.path
import time

# GUI elements are not allowed to be called from additional thread
import Tkinter

import logging


def compute(inst):
    if inst.event.isSet() or inst.error:
        time.sleep(1)
        return

    tag = str(inst.typ) + ": "
    inst.status = "wird vorbereitet..."

    # max. balance
    inst.leerfahrt = inst.find_initial_length(inst.dep)

    inst.status = "Ermittlung der max. Unwucht..."
    inst.maxBal = inst.find_balancing_value_fast(inst.lenMax, inst.dep, inst.angMax)
    logging.debug(tag + "Max balance value is " + str(inst.maxBal))

    if inst.maxBal < inst.balStep:
        logging.error(tag + "Balancing step is too large or maximal imbalance is too small")
        inst.status = "Fehler: Max. Unwucht ist zu klein"
        time.sleep(1)
        return

    if inst.event.isSet() or inst.error:
        time.sleep(1)
        return

    inst.status = "Ermittlung der Werkzeugtiefe..."
    val, ang = inst.find_balancing_value_fast_noAngle(inst.lenStep, inst.dep)
    results = []
    realDepth = inst.find_real_depth(inst.lenStep, inst.dep)
    results.append([inst.lenStep, 0.0, realDepth, val])
    logging.debug(tag + " len, ang, dep, bal " + str(results[-1]))

    if inst.event.isSet() or inst.error:
        time.sleep(1)
        return

    # loop over length
    logging.info(tag + "Computing... (no rotation)")
    incr = 1
    i = 1
    while i < inst.lenMax/inst.lenStep:
        i += incr
        inst.status = "Fraeslaenge " + "{:2.3f}".format(inst.lenStep*i) + "..."
        val, ang = inst.find_balancing_value_fast_noAngle(inst.lenStep*i, inst.dep)
        if val - results[-1][3] < 0.5*inst.balStep:
            incr *= 2.0
            logging.debug(tag + "doubling step to " + str(incr))
        elif val - results[-1][3] > 1.5*inst.balStep:
            if incr > 0.9:
                incr /= 2.0
                logging.debug(tag + "halving step to " + str(incr))
        results.append([inst.lenStep*i, 0.0, inst.find_real_depth(inst.lenStep*i, inst.dep), val])
        logging.debug(tag + " [len, ang, dep, bal] = " + str(results[-1]))

        if inst.event.isSet() or inst.error:
            time.sleep(1)
            return

    # last extreme point
    inst.status = "Max. Fraeslaenge erreicht"
    val, angMaxLen = inst.find_balancing_value_fast_noAngle(inst.lenMax, inst.dep)
    results.append([inst.lenMax, 0.0, inst.find_real_depth(inst.lenMax, inst.dep), val])
    logging.debug(tag + "extreme length point: [len, ang, dep, bal] = " + str(results[-1]))
    realDepth = results[-1][2]

    if inst.event.isSet() or inst.error:
        time.sleep(1)
        return

    # loop over angle
    logging.info(tag + "Computing... Please, wait... (pure rotation)")
    incr = 1
    i = 1
    while i < (inst.angMax - angMaxLen)/inst.angStep:
        i += incr
        inst.status = "Winkel " + "{:2.3f}".format((angMaxLen + i*inst.angStep)*180.0/math.pi) + "..."
        val = inst.find_balancing_value_fast(inst.lenMax, inst.dep, angMaxLen + i*inst.angStep)
        if (val - results[-1][3]) < 0.5*inst.balStep:
            incr *= 2
            logging.debug(tag + "doubling step to " + str(incr))
        elif (val - results[-1][3]) > 1.5*inst.balStep:
            if incr > 0.9:
                incr /= 2.0
                logging.debug(tag + "halving step to " + str(incr))
        results.append([inst.lenMax, i*inst.angStep, realDepth, val])
        logging.debug(tag + " [len, ang, dep, bal] = " + str(results[-1]))

        if inst.event.isSet() or inst.error:
            time.sleep(1)
            return

    logging.info(tag + "Computation finished")
    inst.status = "Berechnung beendet. Speichern..."

    # length angle 'real depth'
    finalResults = []

    #reformatting data
    currIndex = 0
    currStep = 1
    maxBalReal = 0
    while True:
        currBalance = currStep * inst.balStep
        while currIndex+1 < len(results) and results[currIndex+1][3] < currBalance:
            currIndex += 1

        if currIndex+1 >= len(results):
            break

        lmbd = (currBalance - results[currIndex][3]) / (results[currIndex+1][3] - results[currIndex][3])

        pp = [(1.0-lmbd)*a + lmbd*b for a, b in zip(results[currIndex], results[currIndex+1])]
        finalResults.append(pp[:4])
        maxBalReal = currBalance
        currStep += 1

    logging.info("")
    logging.info(tag + "final results:")
    for it in finalResults:
        logging.info(it)
    logging.info("")

    # save results
    if len(finalResults) < 1:
        inst.status = "Fehler: Ungueltige Enddaten"
        time.sleep(1)
        return

    myPath = os.path.dirname(inst.fileName)

    if not os.path.exists(myPath):
        logging.info(tag + "Creating path " + str(myPath))
        inst.mutex.acquire()
        os.makedirs(myPath)
        inst.mutex.release()

    logging.info(tag + "Writing file " + str(inst.fileName))
    # bin-file
    with open(inst.fileName, 'wb') as f:
        rWerkzeug = struct.pack('f', inst.tool)     # rWerkzeug: REAL; (* 0 - 3R; 1 - 3R+1R; 2 - 1R *)
        rDurchmesser = struct.pack('f', inst.workpieceRadius*2.0)   # rDurchmesser: REAL;
        rDichte = struct.pack('f', inst.density)    # rDichte: REAL;
        rTiefe = struct.pack('f', inst.dep)         # rTiefe: REAL; (Werkzeugtiefe)
        rLaenge = struct.pack('f', inst.lenMax)     # rLaenge: REAL;
        rWinkel = struct.pack('f', inst.angMax*180.0/math.pi)   # rWinkel: REAL;
        rWinkel_2 = struct.pack('f', inst.angAux*180.0/math.pi)     # rWinkel_2: REAL;
        rLeerfahrt = struct.pack('f', inst.leerfahrt)           # rLeerfahrt: REAL;
        rMaxUnwucht = struct.pack('f', maxBalReal)             # rMaxUnwucht: REAL;
        rFraestiefe = struct.pack('f', inst.depMeasured)        # Fraestiefe: REAL;
        Reserve_1 = struct.pack('f', inst.balStep)              # Reserve_1: REAL;
        Reserve_2 = struct.pack('f', 0.0)                       # Reserve_2: REAL;

        logging.info(tag + "Header")
        logging.info("rWerkzeug " + str(inst.tool))
        logging.info("rDurchmesser " + str(inst.workpieceRadius*2.0))
        logging.info("rDichte " + str(inst.density))
        logging.info("rTiefe " + str(inst.dep))
        logging.info("rLaenge " + str(inst.lenMax))
        logging.info("rWinkel " + str(inst.angMax*180.0/math.pi))
        logging.info("rWinkel_2 " + str(inst.angAux*180.0/math.pi))
        logging.info("rLeerfahrt " + str(inst.leerfahrt))
        logging.info("rMaxUnwucht " + str(maxBalReal))
        logging.info("rFraestiefe " + str(inst.depMeasured))
        logging.info("Reserve_1 (balStep) " + str(inst.balStep))
        logging.info("Reserve_2 " + str(0.0))

        f.write(rWerkzeug)
        f.write(rDurchmesser)
        f.write(rDichte)
        f.write(rTiefe)
        f.write(rLaenge)
        f.write(rWinkel)
        f.write(rWinkel_2)
        f.write(rLeerfahrt)
        f.write(rMaxUnwucht)
        f.write(rFraestiefe)
        f.write(Reserve_1)
        f.write(Reserve_2)

        for b in finalResults:
            val1 = struct.pack('f', b[0])
            val2 = struct.pack('f', b[1]*180.0/math.pi)
            val3 = struct.pack('f', b[2])
            f.write(val1)
            f.write(val2)
            f.write(val3)

        logging.info(tag + "computation complete")
        inst.status = "Berechnung beendet"
        time.sleep(1)

# --------------------


# Boolean function defining the 3R-tool geometry (true - if point (x,y,z) is inside the tool, false - otherwise)
def belong_to3RTool(x, y, z):
    transitionRadius = 3.0
    radius = 7.0
    bigRadius = radius + transitionRadius
    # to place the tool right below the xy-plane, i.e., no intersection with z>0 half-space,
    # where the workpiece is located
    z += bigRadius
    tmpTorus = radius - math.sqrt(y*y+z*z)  # some auxiliary value

    return((x <= 0 and y*y + z*z <= radius*radius) or                           # first cylinder
           (x <= -transitionRadius and y*y + z*z <= bigRadius*bigRadius) or     # second cylinder
           # transition torus
           (tmpTorus*tmpTorus + (x+transitionRadius)*(x+transitionRadius) <= transitionRadius*transitionRadius))

# --------------------


# Boolean function defining the 3R+1R-tool geometry (true - if point (x,y,z) is inside the tool, false - otherwise)
def belong_to3R1RTool(x, y, z):
    radiusSmall = 17.3 / 2.0
    radiusMid = 19.3 / 2.0
    radiusBig = 10.0
    # to place the tool right below the xy-plane, i.e., no intersection with z>0 half-space,
    # where the workpiece is located
    z += radiusBig

    transRadSmall = 1.0
    torusSmall = radiusSmall - math.sqrt(y*y+z*z)   # some auxiliary value for small torus

    transRadBig = 3.0
    torusBig = (radiusBig - transRadBig) - math.sqrt(y*y+z*z)   # some auxiliary value for big torus

    return((x <= 0 and y*y + z*z <= radiusSmall*radiusSmall ) or   # small cylinder
           (x <= -1.0 and y*y + z*z <= radiusMid*radiusMid) or     # middle cylinder
           (x <= -2.9 and y*y + z*z <= radiusBig*radiusBig) or     # big cylinder
           # small torus
           (torusSmall*torusSmall + (x+transRadSmall)*(x+transRadSmall) <= transRadSmall*transRadSmall) or
           # big torus
           (x < -1.5 and (torusBig*torusBig + (x+transRadBig)*(x+transRadBig) <= transRadBig*transRadBig)))

# --------------------


# Boolean function defining the 1R-tool geometry (true - if point (x,y,z) is inside the tool, false - otherwise)
def belong_to1RTool(x, y, z):
    transitionRadius = 1.0
    radius = 9.0
    bigRadius = radius + transitionRadius
    # to place the tool right below the xy-plane, i.e., no intersection with z>0 half-space,
    # where the workpiece is located
    z += bigRadius
    tmpTorus = radius - math.sqrt(y*y+z*z)  # some auxiliary value

    return((x <= 0 and y*y + z*z <= radius*radius) or                           # first cylinder
           (x <= -transitionRadius and y*y + z*z <= bigRadius*bigRadius) or     # second cylinder
           # transition torus
           (tmpTorus*tmpTorus + (x+transitionRadius)*(x+transitionRadius) <= transitionRadius*transitionRadius))

# --------------------


class Balancing:
    eps = 0.001
    epsmu = 0.000001

    densityDict = {
        "Alu": 0.0027,
        "Titan": 0.0045
    }

    belong_toToolDict = {
        "3R": belong_to3RTool,
        "3R+1R": belong_to3R1RTool,
        "1R": belong_to1RTool
    }

    def __init__(self, eventExit, lock, typ, cmn, spc, stoff, wz):
        #initialization
        try:
            self.typ = typ
            self.tag = str(self.typ) + ": "
            self.event = eventExit
            self.mutex = lock
            self.status = "wird intialisiert..."
            self.error = False

            self.workpieceRadius = float(spc["Durchmesser"].get().replace(',', '.'))/2.0
            self.lenMax = float(spc["Fraeslaenge"].get().strip().replace(',', '.'))
            self.angMax = float(spc["1. Entnahme"].get().strip().replace(',', '.'))/180.0*math.pi
            self.angAux = float(spc["2. Entnahme"].get().strip().replace(',', '.'))/180.0*math.pi
            self.depMeasured = float(spc["Fraestiefe"].get().strip().replace(',', '.'))

            self.fileName = os.path.join("Tabellen", str(wz), cmn["Artikelnummer"].get().strip()+str(typ)+".bin")
            logging.debug(self.tag + "File name: " + self.fileName)
            self.numPoints = int(cmn["Punktenanzahl"])
            self.balStep = cmn["Wuchtenschritt"]
            self.lenStep = cmn["Laengenschritt"]
            self.angStep = cmn["Winkelschritt"]
            self.depStep = cmn["Tiefenschritt"]

            self.density = self.densityDict[stoff]
            # logging.debug(self.tag + "Stoff " + str(self.density))
            if wz == "3R":
                self.tool = 0.0
            elif wz == "3R+1R":
                self.tool = 1.0
            elif wz == "1R":
                self.tool = 2.0
            else:
                self.tool = -1.0
            self.belong_toTool = self.belong_toToolDict[wz]
            # logging.debug(self.tag + "Tool " + str(self.belong_toTool))

            self.balMax = 0.0       # max balance
            self.leerfahrt = 0.0    # some value more

            self.height = None
            if "Nabenhoehe" in spc.keys():
                self.height = float(spc["Nabenhoehe"].get().strip().replace(',', '.'))

        except Exception, inst:
            logging.critical(self.tag + " Exception: " + str(inst))
            logging.critical(self.tag + traceback.format_exc())
            self.status = "Fehler beim Parametereinlesen"
            self.error = True

        # --------------

        # compute tool depth
        minPoint = self.depMeasured     # minimal depth
        maxPoint = 10.0                 # maximal depth

        counter = 0
        while math.fabs(maxPoint-minPoint) > self.epsmu:
            midPoint = (maxPoint+minPoint) / 2.0    # bisection of the angle interval
            if self.find_real_depth(self.lenMax, midPoint) < self.depMeasured:
                minPoint = midPoint
            else:
                maxPoint = midPoint
            counter += 1

        self.dep = (maxPoint+minPoint)/2.0
        logging.info(self.tag + "Tool depth to achieve given measured depth is " + str(self.dep))

        # show computed value in GUI
        wzt = spc["Werkzeugtiefe"]
        wzt.configure(state='normal')
        wzt.delete(0, Tkinter.END)
        wzt.insert(0, str(self.dep))
        wzt.configure(state='disabled')

    # --------------------

    # returns true if given point belongs to the workpiece, returns false otherwise
    # Note: (0,0,0) belongs to the workpiece
    def belong_toWorkpiece(self, x, y, z):
        return(z >= 0 and     # upper half-space and disk of radius 'workpieceRadius' in the right half-space
               (x-self.workpieceRadius)*(x-self.workpieceRadius) + y*y <= self.workpieceRadius*self.workpieceRadius)

    # --------------------

    # finding a length corresponding to initial position of the tool at depth dep, such that it touches the workpiece at (0,0,0)
    def find_initial_length(self, dep):
        # logging.debug(self.tag + "Starting find_initial_length...")
        lPoint = 0.0                    # minimal length is 0.0f
        rPoint = self.workpieceRadius   # maximal length is the radius

        # if self.belong_toTool(-lPoint,0.0,-dep): #if tool/workpiece intersection is not empty from the very beginning, the solution is found (but it could also be a bug in tool geometry)
        #     logging.debug(self.tag + "find_initial_length: Check the default tool position, since (len,dep)=(0.0, " + str(dep) +  ") touches the workpiece")
        #     return 0.0

        if not self.belong_toTool(-rPoint, 0.0, -dep):  # maximal length does not give tool/workpiece collision
            logging.error(self.tag + "Error: find_initial_length: The length " + str(rPoint) + " is not sufficient. Please, check the tool geometry")
            self.status = "Laenge " + str(rPoint) + " reicht nicht aus"
            self.error = True
            return rPoint

        counter = 0
        while math.fabs(rPoint-lPoint) > self.epsmu:
            midPoint = (rPoint+lPoint) / 2.0    # bisection of the angle interval
            if self.belong_toTool(-midPoint, 0.0, -dep):
                rPoint = midPoint
            else:
                lPoint = midPoint
            counter += 1

        return (rPoint+lPoint)/2.0

    # --------------------

    # finding an angle corresponding to a pure insertion of the tool at length 'len' and depth 'dep' (without rotating the workpiece)
    def find_initial_angle(self, len, dep):
        # logging.debug(self.tag + "Starting find_initial_angle...");
        lAngle = 0.0                    # angle in radians; corresponds to point (0,0,0)
        rAngle = math.pi/180.0 * 90.0   # angle in radians; corresponds to point (workpieceRadius,workpieceRadius,0)
        if not self.belong_toTool(-len, 0.0, -dep):  # if tool is not inside the workpiece, there is nothing to compute
            logging.error(self.tag + "Error: find_initial_angle: The tool is not inside the workpiece at (len, dep)=(" + str(len) + ", " + str(dep) + ")")
            self.status = "Fehler bei der Berechnung des Anfangswinkels"
            self.error = True
            return 0.0

        if self.belong_toTool(self.workpieceRadius-len, self.workpieceRadius, -dep):  # if tool is not inside the workpiece, there is nothing to compute
            logging.error(self.tag + "Error: find_initial_angle: The length " + str(len) + " is too large")
            self.error = True
            return 0.0

        counter = 0
        while math.fabs(rAngle-lAngle) > self.epsmu:
            midAngle = (rAngle + lAngle) / 2.0        #bisection of the angle interval
            midX = self.workpieceRadius*(-math.cos(midAngle) + 1.0)
            midY = self.workpieceRadius*math.sin(midAngle)
            if self.belong_toTool(midX-len, midY, -dep):
                lAngle = midAngle
            else:
                rAngle = midAngle
            counter += 1

        # logging.debug(self.tag + "find_initial_angle finished after " + str(counter) + " iterations")
        # logging.debug(self.tag + "Initial angle value is " + str(rAngle+lAngle) + " = " + str((rAngle+lAngle)*180.0/math.pi) + " grad")
        return rAngle+lAngle

    # --------------------

    def find_real_depth(self, len, dep):
        len += self.find_initial_length(dep)    # length correction

        if self.belong_toTool(-len, 0.0, 0.0):
            return dep
        xa, xb, xc = -dep, 0.0, 0.0
        while xb-xa > self.eps:
            xc = (xa + xb) / 2.0
            if self.belong_toTool(-len, 0.0, xc):
                xa = xc
            else:
                xb = xc

        return dep + (xa + xb) / 2.0

    # --------------------

    # finding balancing value for given length 'len', depth 'dep', and angle 'phi'
    def find_balancing_value_fast(self, len, dep, phi):
        if phi > math.pi*1.5:
            logging.error(self.tag + "find_balancing_value_fast: Too large angle " + str(phi) + "=" + str(phi*180.0/math.pi))
            self.status = "Winkel ist zu gross"
            self.error = True
            return 0.0

        length = len
        depth = dep
        angle = phi

        initLength = self.find_initial_length(dep)
        len += initLength                                  #length correction

        initAngle = self.find_initial_angle(len,dep)
        if initAngle > phi:
            logging.error(self.tag + "find_balancing_value_fast: Too small angle " + str(phi) + "=" + str(phi*180.0/math.pi) + " vs. initAngle " + str(initAngle))
            self.status = "Winkel ist zu klein"
            return 0.0

        phi0 = (angle - initAngle) / 2.0        #angle corridor [-phi0,phi0] is regular case; otherwise -- boundary case

        # bounding box for points throwing
        boxMonteCarlo0 = (0.0, -self.workpieceRadius*math. sin(angle/2.0), 0.0)
        boxMonteCarlo1 = (length*math.cos(angle/2.0) + self.workpieceRadius*(-math.cos(angle/2.0) + 1.0),
                          self.workpieceRadius*math.sin(angle/2.0), depth)

        value = 0.0

        for i in range(self.numPoints):
            # get random point
            x = random.uniform(boxMonteCarlo0[0],boxMonteCarlo1[0])
            y = random.uniform(boxMonteCarlo0[1],boxMonteCarlo1[1])
            z = random.uniform(boxMonteCarlo0[2],boxMonteCarlo1[2])

            if not self.belong_toWorkpiece(x, y, z):
                continue    # the point is outside the workpiece

            # reduce general case to the case without rotation
            tmpLen = math.sqrt((self.workpieceRadius - x)*(self.workpieceRadius - x) + y*y)   #distance to the workpiece center
            if tmpLen < self.workpieceRadius - len:
                continue            #definitely outside of the tool position: zero contribution

            angCos = (self.workpieceRadius - x) / tmpLen
            if angCos > math.cos(phi0):   # regular case: rotate by the angle phi/2.0f, i.e., set y=0
                x = self.workpieceRadius - tmpLen
                y = 0.0
            else:                    # boundary case: rotate by the angle phi0
                tmpx = (x-self.workpieceRadius)*math.cos(phi0) - math.sin(phi0)*math.fabs(y)
                y = -(x-self.workpieceRadius)*math.sin(phi0) - math.cos(phi0)*math.fabs(y)  # can also be sqrt(tmpLen*tmpLen - x*x);
                x = self.workpieceRadius + tmpx

            # integrate
            if self.belong_toTool(x-len, y, z-dep):     # if the current point belongs to the integration domain
                value += tmpLen * angCos    # integrated function is 'r' (in Cartesian coordinates) * cosine to the main axis

        value /= self.numPoints             # Monte-Carlo averaging by number of samples
        tmp = tuple(map(lambda xx, yy: xx - yy, boxMonteCarlo1, boxMonteCarlo0))
        value *= tmp[0]*tmp[1]*tmp[2]       # Monte-Carlo volume scaling
        value *= self.density               # density factor

        return(value)

    # --------------------

    # finding balancing value for given length 'len', depth 'dep', and angle 'phi'
    def find_balancing_value_fast_noAngle(self, len, dep):
        length = len
        depth = dep

        initLength = self.find_initial_length(dep)
        len += initLength                                  # length correction

        initAngle = self.find_initial_angle(len,dep)
        phi = initAngle
        angle = initAngle

        phi0 = (angle - initAngle) / 2.0        #angle corridor [-phi0,phi0] is regular case; otherwise -- boundary case

        # bounding box for points throwing
        boxMonteCarlo0 = (0.0,-self.workpieceRadius*math.sin(angle/2.0), 0.0)
        boxMonteCarlo1 = (length*math.cos(angle/2.0) + self.workpieceRadius*(-math.cos(angle/2.0) + 1.0),
                          self.workpieceRadius*math.sin(angle/2.0), depth)

        value = 0.0

        for i in range(self.numPoints):
            # get random point
            x = random.uniform(boxMonteCarlo0[0],boxMonteCarlo1[0])
            y = random.uniform(boxMonteCarlo0[1],boxMonteCarlo1[1])
            z = random.uniform(boxMonteCarlo0[2],boxMonteCarlo1[2])

            if not self.belong_toWorkpiece(x, y, z):
                continue    # the point is outside the workpiece

            # reduce general case to the case without rotation
            tmpLen = math.sqrt((self.workpieceRadius - x)*(self.workpieceRadius - x) + y*y)   # distance to the workpiece center
            if tmpLen < self.workpieceRadius - len:
                continue            # definitely outside of the tool position: zero contribution

            angCos = (self.workpieceRadius - x) / tmpLen
            if angCos > math.cos(phi0):   # regular case: rotate by the angle phi/2.0f, i.e., set y=0
                x = self.workpieceRadius - tmpLen
                y = 0.0
            else:                    # boundary case: rotate by the angle phi0
                tmpx = (x-self.workpieceRadius)*math.cos(phi0) - math.sin(phi0)*math.fabs(y)
                y = -(x-self.workpieceRadius)*math.sin(phi0) - math.cos(phi0)*math.fabs(y)  # can also be sqrt(tmpLen*tmpLen - x*x);
                x = self.workpieceRadius + tmpx

            # integrate
            if self.belong_toTool(x-len, y, z-dep):     # if the current point belongs to the integration domain
                value += tmpLen * angCos    # integrated function is 'r' (in Cartesian coordinates) * cosine to the main axis

        value /= self.numPoints      # Monte-Carlo averaging by number of samples
        tmp = tuple(map(lambda x, y: x - y, boxMonteCarlo1, boxMonteCarlo0))
        value *= tmp[0]*tmp[1]*tmp[2]     # Monte-Carlo volume scaling
        value *= self.density             # density factor

        return value, phi
