# -*- mode: python -*-
a = Analysis(['auswuchttabellenerstellung.py'],
             pathex=['C:\\bitbucket\\projects\\small\\AuswuchtTabelle'],
             hiddenimports=['pkg_resources'],
             hookspath=None,
             runtime_hooks=None)
a.datas += [('logo.png','C:\\bitbucket\\projects\\small\\AuswuchtTabelle\\data\\logo.png','DATA')]
for d in a.datas:
    if 'pyconfig' in d[0]: 
        a.datas.remove(d)
        break
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='auswuchttabellenerstellung.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False, 
		icon='data\\impeller.ico')
