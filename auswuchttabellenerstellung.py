# author: Dr. Vladimir Molchanov
# date: 23.07.2015

import Tkinter
import threading
import time
import ttk
import tkSimpleDialog
import tkFileDialog
import tkMessageBox
import os.path
import struct
import traceback
import sys

import MySQLdb as mdb
import ftplib

import PIL.Image
import PIL.ImageTk

import compute

import logging
logname = 'auswuchttabellenerstellung.log'
logging.basicConfig(filename=logname, level=logging.DEBUG, format='%(asctime)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())
with open(logname, 'w'):
    pass

fields = ("Durchmesser", "Fraeslaenge", "Fraestiefe")
fieldsAngle = ("1. Entnahme", "2. Entnahme")

root = None
rootSize = None

commonEntries = {
    "Punktenanzahl": 1000000,
    "Tiefenschritt": 0.01,
    "Winkelschritt": 0.025,
    "Laengenschritt": 0.025,
    "Wuchtenschritt": 0.001
}
rueckenEntries = None
nabeEntries = None

aboutToExit = threading.Event()
mutex = threading.Lock()
watchThread = None

# gui variables
progress = 0
material = None
tool = None
status = ""
statusNabe = ""
statusRuecken = ""

nabeBalance = None
rueckenBalance = None


# ------------------------------
# this function repeats implementation of synchron() below. Indeed, the code can be written better.
# It is only for testing and debugging. Sorry.

def synchronSingle():
    tag = "synchronSingle: "
    logging.debug("")
    logging.info("_"*30)
    machines = {}
    try:
        try:
            logging.info(tag + "Open an SQL-connection")
            conn = mdb.connect(host="192.168.1.9", user="watch_client", passwd="turbo",
                               db="prae-data", connect_timeout=10000)
            try:
                curr = conn.cursor()
                sql = "SELECT `Name`, `ip`, `user`, `passwd` FROM `maschine`"
                logging.debug("Query is: " + sql)
                curr.execute(sql)
                res = curr.fetchall()
                logging.debug(res)
                for row in res:
                    machines[row[0]] = row[1:]
                logging.debug(machines)

            except Exception, inst:
                logging.critical(tag + "Exception in SQL; rolling back")
                logging.critical(tag + "Exception: " + str(inst))
        finally:
            logging.info(tag + "Close an SQL-connection")
            if conn:
                conn.close()
    except Exception, inst:
        logging.critical(tag + "Exception: Can not connect to SQL-Server. Check network. " + str(inst))
        tkMessageBox.showerror("Fehlermeldung", "Keine Verbindung zur SQL-Datenbank. Ueberpruefen Sie Netzwerk.")
        return

    def send_data(dt, show, fls):
        tag = "send_data: "
        res = 1
        logging.debug(tag + "ip, usr, psw: " + str(dt))
        try:
            ftp = ftplib.FTP(dt[0])
            ftp.login(user=dt[1], passwd=dt[2])
            try:
                logging.info(tag + "Login to FTP succeeded")
                logging.debug("Content on FTP in current folder:" + str(ftp.nlst()))

                # targetFolder = "Tabellen"
                # if not targetFolder in ftp.nlst():
                #    ftp.mkd(targetFolder)
                # ftp.cwd(targetFolder)
                # logging.debug(tag + "Content on FTP in folder 'Tabellen':" + str(ftp.nlst()))

                def synch_folder(myftp, targetFolder, files):
                    logging.debug("")
                    logging.debug(tag + "Synchronizing selected files with FTP")
                    if targetFolder not in myftp.nlst():
                        myftp.mkd(targetFolder)
                    myftp.cwd(targetFolder)

                    logging.debug(tag + "Current content of FTP: " + str(myftp.nlst()))
                    try:
                        logging.debug(tag + "Local files for synchronization: " + str(files))
                        for fpath in files:
                            logging.debug(tag + "Operating item " + str(fpath))
                            if os.path.isfile(fpath):
                                logging.debug(tag + "Copying file " + str(fpath) + " to FTP-Folder " + targetFolder)
                                fh = open(fpath, 'rb')
                                myDir, f = os.path.split(fpath)
                                myftp.storbinary('STOR %s' % f, fh)
                                fh.close()
                        logging.debug(tag + "Done")
                        logging.debug(tag + "Final content of FTP-folder" + str(myftp.nlst()))
                        myftp.cwd("..")
                        return(1)
                    except Exception, inst:
                        logging.critical(tag + "Fehlermeldung: Datensynchronisierung fehlgeschlagen: " + str(inst))
                        return(0)

                if "/3R+1R/" in fls[0]:
                    logging.debug(tag + "Detected folder name: '3R+1R'")
                    res *= synch_folder(ftp, "3R+1R", fls)
                elif "/3R/" in fls[0]:
                    logging.debug(tag + "Detected folder name: '3R'")
                    res *= synch_folder(ftp, "3R", fls)
                elif "/1R/" in fls[0]:
                    logging.debug(tag + "Detected folder name: '1R'")
                    res *= synch_folder(ftp, "1R", fls)
                else:
                    logging.critical(tag + "Can not parse " + fls[0])
                    tkMessageBox.showinfo("Fehler", "Fehler bei der Datensynchronisierung, sieh Logging-Datei")
                    res = 0

                if show == 1 and res == 1:
                    tkMessageBox.showinfo("Fertig", "Datensynchronisierung erfolgreich abgeschlossen")
            except:
                tkMessageBox.showerror("Fehlermeldung", "Datensynchronisierung fehlgeschlagen")
                res = 0

            finally:
                try:
                    ftp.quit()
                except:
                    ftp.close()
        except:
            tkMessageBox.showerror("Fehlermeldung",
                                   "Datensynchronisierung fehlgeschlagen. Keine Verbindung zu " + str(dt[0]))
            res = 0
        return res

    # -------------------------
    def send_all(m, fls):
        res = 1
        for k in m.keys():
            try:
                res *= send_data(m[k], 0, fls)
            except:
                pass
        if res > 0:
            tkMessageBox.showinfo("Fertig", "Gewaelte Daten wurden synchronisiert")
        else:
            tkMessageBox.showinfo("Fertig", "Daten wurden mit Fehler synchronisiert")

    files = tkFileDialog.askopenfilenames(title='Select File(s)')
    global root
    if files is not None and files:
        files = root.tk.splitlist(files)
        logging.debug(tag + "selected files: " + str(files))
    else:
        logging.debug(tag + "No selected files. Quitting...")
        return

    rt = Tkinter.Tk()
    rt.wm_title('Datensynchronisierung (einzeln)')
    rt.resizable(0, 0)
    but = Tkinter.Button(rt,  width=30, text="Alle Maschinen", command=lambda: send_all(machines, files),
                         padx=5, pady=5)
    but.pack()
    for k in machines.keys():
        but = Tkinter.Button(rt, width=30, text=str(k), command=lambda m=k: send_data(machines[m], 1, files),
                             padx=5, pady=5)
        but.pack()
    place_side(rt)
    rt.mainloop()
    rt.quit()

# ------------------------------


def synchron():
    logging.debug("")
    logging.info("_"*30)
    machines = {}
    try:
        try:
            logging.info("Open an SQL-connection")
            conn = mdb.connect(host="192.168.1.9", user="watch_client", passwd="turbo",
                               db="prae-data", connect_timeout=10000)
            try:
                curr = conn.cursor()
                sql = "SELECT `Name`, `ip`, `user`, `passwd` FROM `maschine`"
                logging.debug("Query is: " + sql)
                curr.execute(sql)
                res = curr.fetchall()
                logging.debug(res)
                for row in res:
                    machines[row[0]] = row[1:]
                logging.debug(machines)

            except Exception, inst:
                logging.critical("Exception in SQL; rolling back")
                logging.critical("Exception: " + str(inst))
        finally:
            logging.info("Close an SQL-connection")
            if conn:
                conn.close()
    except Exception, inst:
        logging.critical("Exception: Can not connect to SQL-Server. Check network. " + str(inst))
        tkMessageBox.showerror("Fehlermeldung", "Keine Verbindung zur SQL-Datenbank. Ueberpruefen Sie Netzwerk.")
        return

    def send_data(dt, show):
        tag = "send_data: "
        res = 1
        logging.debug(tag + "ip, usr, psw: " + str(dt))
        try:
            ftp = ftplib.FTP(dt[0])
            ftp.login(user=dt[1], passwd=dt[2])
            try:
                logging.info(tag + "Login to FTP succeeded")
                logging.debug("Content on FTP in current folder:" + str(ftp.nlst()))

                # targetFolder = "Tabellen"
                # if not targetFolder in ftp.nlst():
                #    ftp.mkd(targetFolder)
                # ftp.cwd(targetFolder)
                # logging.debug(tag + "Content on FTP in folder 'Tabellen':" + str(ftp.nlst()))

                def synch_folder(myftp, locPath, targetFolder):
                    logging.debug(tag + "\nSynchronizing " + locPath + " with FTP")
                    if targetFolder not in myftp.nlst():
                        myftp.mkd(targetFolder)
                    myftp.cwd(targetFolder)

                    logging.debug(tag + "Current content of FTP: " + str(myftp.nlst()))
                    try:
                        files = os.listdir(locPath)
                        logging.debug(tag + "Local files: " + str(files))
                        for f in files:
                            logging.debug(tag + "Operating item " + str(f))
                            fpath = locPath + r'\{}'.format(f)
                            if os.path.isfile(fpath):
                                logging.debug(tag + "Copying file " + str(fpath) + " to FTP-Folder " + targetFolder)
                                fh = open(fpath, 'rb')
                                myftp.storbinary('STOR %s' % f, fh)
                                fh.close()
                        logging.debug(tag + "Done")
                        logging.debug(tag + "Final content of FTP-folder" + str(myftp.nlst()))
                        myftp.cwd("..")
                        return 1
                    except Exception, inst:
                        logging.debug(tag + "Fehlermeldung: Datensynchronisierung fehlgeschlagen: " + str(inst))
                        return 0

                res *= synch_folder(ftp, "Tabellen/3R", "3R")
                res *= synch_folder(ftp, "Tabellen/3R+1R", "3R+1R")
                res *= synch_folder(ftp, "Tabellen/1R", "1R")

                if show == 1:
                    tkMessageBox.showinfo("Fertig", "Datensynchronisierung erfolgreich abgeschlossen")
            except:
                tkMessageBox.showerror("Fehlermeldung", "Datensynchronisierung fehlgeschlagen")
                res = 0

            finally:
                try:
                    ftp.quit()
                except:
                    ftp.close()
        except:
            tkMessageBox.showerror("Fehlermeldung", "Datensynchronisierung fehlgeschlagen. Keine Verbindung zu " + str(dt[0]))
            res = 0
        return res

    def send_all(m):
        res = 1
        for k in m.keys():
            try:
                res *= send_data(m[k], 0)
            except:
                pass
        if res > 0:
            tkMessageBox.showinfo("Fertig", "Alle Daten wurden synchronisiert")
        else:
            tkMessageBox.showinfo("Fertig", "Daten wurden mit Fehler synchronisiert")

    rt = Tkinter.Tk()
    rt.wm_title('Datensynchronisierung')
    rt.resizable(0, 0)
    but = Tkinter.Button(rt, width=30, text="Alle Maschinen", command=lambda: send_all(machines), padx=5, pady=5)
    but.pack()
    for k in machines.keys():
        but = Tkinter.Button(rt, width=30, text=str(k), command=lambda m=k: send_data(machines[m], 1), padx=5, pady=5)
        but.pack()
    place_side(rt)
    rt.mainloop()
    rt.quit()

# ------------------------------


def admin():
    logging.debug("")
    logging.info("_"*30)
    psw = tkSimpleDialog.askstring("Kennwort", "Kennwort eingeben:", show='*')
    if psw == "turbo":
        logging.debug("Password accepted")
        rt = Tkinter.Tk()
        rt.resizable(0, 0)

        global commonEntries
        entr = {}

        def add_entry(txt, rowNum):
            row = Tkinter.Frame(rt)
            lab = Tkinter.Label(row, width=15, text=txt, anchor='w')
            ent = Tkinter.Entry(row)
            ent.insert(0, str(commonEntries[txt]))
            row.grid(row=rowNum, column=1, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)
            lab.pack(side=Tkinter.LEFT)
            ent.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)
            entr[txt] = ent

        rt.wm_title('Zusaetzliche Parameter')
        add_entry("Punktenanzahl", 0)
        add_entry("Tiefenschritt", 1)
        add_entry("Winkelschritt", 2)
        add_entry("Laengenschritt", 3)
        add_entry("Wuchtenschritt", 4)

        def onclick(dic):
            global commonEntries
            #logging.debug(dic)
            for k in dic.keys():
                try:
                    val = float(dic[k].get().strip().replace(',', '.'))
                    commonEntries[k] = val
                except Exception, inst:
                    logging.critical("Exception: " + str(inst))
                    logging.critical(traceback.format_exc())
                    tkMessageBox.showerror("Fehlermeldung", "Konnte den Wert von " + str(k) + " nicht einlesen")
            rt.destroy()

        Tkinter.Button(rt, command=lambda: onclick(entr), text = 'OK').grid(row=5, column=1, columnspan=1,
                                                                            rowspan=1, sticky='NS', padx=5, pady=5)

        rt.mainloop()
        rt.quit()

# ------------------------------


def read():
    logging.debug("")
    logging.info("_"*30)
    if len(threading.enumerate()) > 1:
        tkMessageBox.showerror("Fehlermeldung", "Erst Berechnung abbrechen!")
        return

    myPath = tkFileDialog.askopenfilename(title='Detei auswaehlen', filetypes=[("Bin-Deteien", "*.bin")])
    # logging.debug("myPath " + str(myPath))
    if myPath is not None and myPath:
        myDir, myFile = os.path.split(myPath)

        global commonEntries, nabeEntries, rueckenEntries
        if "Nabe" in myFile:
            myDict = nabeEntries
            artNr = myFile.split("Nabe")[0]
        elif "Ruecken" in myFile:
            myDict = rueckenEntries
            artNr = myFile.split("Ruecken")[0]
        else:
            logging.warn("Can not parse filename " + myFile)
            tkMessageBox.showerror("Fehlermeldung", "Ungueltiger Dateiname")
            return

        commonEntries["Artikelnummer"].delete(0, Tkinter.END)
        commonEntries["Artikelnummer"].insert(0, artNr)
        logging.info("Artikelnummer " + str(commonEntries["Artikelnummer"].get().strip()))

        global material, tool
        with open(myPath, "rb") as f:
            vals = struct.unpack('f'*12, f.read(4*12))

            if vals[0] < 0.5:
                tool.set("3R")
            elif vals[0] < 1.5:
                tool.set("3R+1R")
            else:
                tool.set("1R")

            myDict["Durchmesser"].delete(0, Tkinter.END)
            myDict["Durchmesser"].insert(0, str(vals[1]))

            if vals[2] < 0.03:
                material.set("Alu")
            else:
                material.set("Titan")

            myDict["Werkzeugtiefe"].delete(0, Tkinter.END)
            myDict["Werkzeugtiefe"].insert(0, str(vals[3]))

            myDict["Fraeslaenge"].delete(0, Tkinter.END)
            myDict["Fraeslaenge"].insert(0, str(vals[4]))

            myDict["1. Entnahme"].delete(0, Tkinter.END)
            myDict["1. Entnahme"].insert(0, str(vals[5]))

            myDict["2. Entnahme"].delete(0, Tkinter.END)
            myDict["2. Entnahme"].insert(0, str(vals[6]))

            myDict["Fraestiefe"].delete(0, Tkinter.END)
            myDict["Fraestiefe"].insert(0, str(vals[9]))

            logging.debug("Werkzeug " + str(vals[0]))
            logging.debug("Durchmesser " + str(vals[1]))
            logging.debug("Dichte " + str(vals[2]))
            logging.debug("Tiefe " + str(vals[3]))
            logging.debug("Laenge " + str(vals[4]))
            logging.debug("Winkel " + str(vals[5]))
            logging.debug("Winkel_2 " + str(vals[6]))
            logging.debug("Leerfahrt " + str(vals[7]))
            logging.debug("Max. Unwucht " + str(vals[8]))
            logging.debug("Fraesetiefe " + str(vals[9]))
            logging.debug("Nix 1 " + str(vals[10]))
            logging.debug("Nix 2 " + str(vals[11]))

        cnt = 0
        with open(myPath, "rb") as f:
            try:
                while True:
                    struct.unpack('f', f.read(4))
                    cnt += 1
            except:
                pass
        logging.debug("Number of floats in the file " + str(cnt))
        logging.debug("Lines for balancing " + str((cnt - 12) / 3))

        logging.info("read complete")
        logging.info("")

# ------------------------------


def watch(t1, t2, but, pB, prog, stat):
    global aboutToExit, nabeBalance, rueckenBalance
    while True:
        statusNabe.set(nabeBalance.status)
        statusRuecken.set(rueckenBalance.status)
        t1.join(1)
        if not t1.isAlive():
            break
        if aboutToExit.isSet():
            return

    while True:
        statusNabe.set(nabeBalance.status)
        statusRuecken.set(rueckenBalance.status)
        t2.join(1)
        if not t2.isAlive():
            break
        if aboutToExit.isSet():
            return

    logging.debug("")
    logging.info("_"*30)
    logging.info("Threads joined")

    aboutToExit.clear()
    but["text"] = "erstellen"
    statusNabe.set(nabeBalance.status)
    statusRuecken.set(rueckenBalance.status)
    pB.stop()
    prog.set(0)
    stat.set("")

# ------------------------------


def isfloat(value):
    try:
        fvalue = float(value)
        return fvalue >= 0.0
    except ValueError:
        return False

# ------------------------------


def run(progBar, button):
    global progress, aboutToExit, status
    if "erstellen" in button["text"]:
        logging.debug("")
        logging.info("_"*30)
        aboutToExit.clear()

        global commonEntries, rueckenEntries, nabeEntries
        if not commonEntries["Artikelnummer"].get().strip():
            tkMessageBox.showerror("Fehlermeldung", "Keine gueltige Artikelnummer angegeben")
            return
        for k in rueckenEntries.keys():
            if k == "Werkzeugtiefe":
                continue
            num = rueckenEntries[k].get().strip().replace(',', '.')
            if not num or not isfloat(num):
                tkMessageBox.showerror("Fehlermeldung", "Keine gueltige Angabe fuer " + k + " bei Ruecken")
                return
        for k in nabeEntries.keys():
            if k == "Werkzeugtiefe" or k == "Nabenhoehe":
                continue
            num = nabeEntries[k].get().strip().replace(',', '.')
            if not num or not isfloat(num):
                tkMessageBox.showerror("Fehlermeldung", "Keine gueltige Angabe fuer " + k + " bei Nabe")
                return

        global tool
        if tool.get() == "3R+1R" and float(nabeEntries["Fraeslaenge"].get().strip().replace(',', '.')) > 1.5:
            if not tkMessageBox.askyesno('Warnung',
                                         'Zu grosse Nabenfraeslaenge fuers Tool 3R+1R.\n Das Werkstueck wird mit dem grossen Radius gefraest.\nTrotzdem berechnen?'):
                return

        button["text"] = "abbrechen"
        progress.set(0)
        progBar.start()
        status.set("Berechnung laeuft...")

        global material, mutex, nabeBalance, rueckenBalance
        nabeBalance = compute.Balancing(aboutToExit, mutex, "Nabe", commonEntries, nabeEntries,
                                        material.get(), tool.get())
        if nabeBalance.height is not None:
            if nabeBalance.height < nabeBalance.dep:
                logging.warning("Too large depth and/or laength (Nabe)")
                tkMessageBox.showerror("Fehlermeldung",
                                       "Zu grosse Fraestiefe/Fraeslaenge (Nabe) eingegeben. Moegliche Beschaedigung der Schaufeln")
                button["text"] = "erstellen"
                progBar.stop()
                progress.set(0)
                status.set("bereit")
                return
        nabeThread = threading.Thread(name='Nabe', target=compute.compute, args=(nabeBalance,))
        nabeThread.start()

        rueckenBalance = compute.Balancing(aboutToExit, mutex, "Ruecken", commonEntries,
                                           rueckenEntries, material.get(), tool.get())
        rueckenThread = threading.Thread(name='Ruecken', target=compute.compute, args=(rueckenBalance,))
        rueckenThread.start()

        global watchThread
        watchThread = threading.Thread(name='Watch', target=watch,
                                       args=(nabeThread, rueckenThread, button, progBar, progress, status,))
        watchThread.start()

    else:
        status.set("anhalten...")
        terminate_threads()
        button["text"] = "erstellen"
        progBar.stop()
        progress.set(0)
        status.set("abgebrochen")
        statusNabe.set("abgebrochen")
        statusRuecken.set("abgebrochen")
        logging.debug("")
        logging.info("_"*30)

# ------------------------------


def populate(rt):
    entries = {}
    for field in fields:
        row = Tkinter.Frame(rt)
        lab = Tkinter.Label(row, width=15, text=field + ", mm", anchor='w')
        ent = Tkinter.Entry(row)
        row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
        lab.pack(side=Tkinter.LEFT)
        ent.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)
        entries[field] = ent

    field = "Werkzeugtiefe"
    row = Tkinter.Frame(rt)
    lab = Tkinter.Label(row, width=15, text=field + ", mm", anchor='w')
    ent = Tkinter.Entry(row)
    ent.configure(state='disabled')
    row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
    lab.pack(side=Tkinter.LEFT)
    ent.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)
    entries[field] = ent

    lbl = Tkinter.Label(rt, width=20, text="Max. Abnahmewinkel", anchor='w')
    lbl.pack(side=Tkinter.TOP)

    for field in fieldsAngle:
        row = Tkinter.Frame(rt)
        lab = Tkinter.Label(row, width=15, text=field + ", grad", anchor='w')
        ent = Tkinter.Entry(row)
        row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
        lab.pack(side=Tkinter.LEFT)
        ent.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)
        entries[field] = ent

    return entries

# ------------------------------


def resource_path():
    datadir = "data"
    if not hasattr(sys, "frozen"):   # not packed
        datadir = os.path.join(os.path.dirname(__file__), datadir)
    elif "_MEIPASS2" in os.environ:   # one-file temp's directory
        datadir = os.path.join(os.environ["_MEIPASS2"], datadir)
    else:   # one-dir
        datadir = os.path.join(os.path.dirname(sys.argv[0]), datadir)
    return datadir


def resource_path2(relative):
    return os.path.join(
        os.environ.get(
            "_MEIPASS2",
            os.path.abspath(".")
        ),
        relative
    )

# ------------------------------


def gui(form):
    global commonEntries

    txt = "Artikelnummer"
    row = Tkinter.Frame(form)
    lab = Tkinter.Label(row, width=15, text=txt, anchor='w')
    ent = Tkinter.Entry(row)
    row.grid(row=1, column=1, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)
    lab.pack(side=Tkinter.LEFT)
    ent.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)
    commonEntries[txt] = ent

    path = resource_path2("logo.png")
    # path = os.path.join(resource_path(), "logo.png")
    logging.debug("path to logo " + str(path))
    minc = PIL.Image.open(path)
    minc.thumbnail((250, 250), PIL.Image.ANTIALIAS)
    mincol = PIL.ImageTk.PhotoImage(minc)
    logo = Tkinter.Label(form, image=mincol)
    logo.image = mincol
    logo.grid(row=1, column=2, columnspan=2, rowspan=2, sticky='NS', padx=5, pady=5)

    global material, tool

    row = Tkinter.Frame(form)
    Tkinter.Label(row, width=15, text="Material", anchor='w').pack(side=Tkinter.LEFT)
    Tkinter.Radiobutton(row, text="Alu", padx=5, variable=material, value="Alu").pack(side=Tkinter.LEFT)
    Tkinter.Radiobutton(row, text="Titan", padx=5, variable=material, value="Titan").pack(side=Tkinter.LEFT)
    row.grid(row=2, column=1, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)

    row = Tkinter.Frame(form)
    Tkinter.Label(row, width=15, text="Werkzeug", anchor='w').pack(side=Tkinter.LEFT)
    Tkinter.Radiobutton(row, text="3R", padx=5, variable=tool, value="3R").pack(side=Tkinter.LEFT)
    Tkinter.Radiobutton(row, text="3R+1R", padx=5, variable=tool, value="3R+1R").pack(side=Tkinter.LEFT)
    Tkinter.Radiobutton(row, text="1R", padx=5, variable=tool, value="1R").pack(side=Tkinter.LEFT)
    row.grid(row=3, column=1, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)

    global rueckenEntries, nabeEntries

    ruecken = Tkinter.LabelFrame(form, text=" Ruecken ")
    ruecken.grid(row=4, column=1, columnspan=1, rowspan=6, sticky='NS', padx=5, pady=5)
    rueckenEntries = populate(ruecken)

    row = Tkinter.Frame(ruecken)
    lab = Tkinter.Label(row, width=15, text="", anchor='w')
    row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
    lab.pack(side=Tkinter.LEFT)

    row = Tkinter.Frame(ruecken)
    lab = Tkinter.Label(row, textvariable=statusRuecken, fg="blue", font="Times 12 bold")
    row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
    lab.pack()

    nabe = Tkinter.LabelFrame(form, text=" Nabe ")
    nabe.grid(row=4, column=2, columnspan=1, rowspan=6, sticky='NS', padx=5, pady=5)
    nabeEntries = populate(nabe)

    txt = "Nabenhoehe"
    row = Tkinter.Frame(nabe)
    lab = Tkinter.Label(row, width=15, text=txt + ", mm", anchor='w')
    ent = Tkinter.Entry(row)
    row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
    lab.pack(side=Tkinter.LEFT)
    ent.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)
    nabeEntries[txt] = ent

    row = Tkinter.Frame(nabe)
    lab = Tkinter.Label(row, textvariable=statusNabe, fg="blue", font="Times 12 bold")
    row.pack(side=Tkinter.TOP, fill=Tkinter.X, padx=5, pady=5)
    lab.pack()

    row = Tkinter.Frame(form)
    synchAll = Tkinter.Button(row, text='Datensynch. alle', command=synchron)
    synchSingle = Tkinter.Button(row, text='Datensynch. einzeln', command=synchronSingle)
    row.grid(row=10, column=2, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)
    synchAll.pack(side=Tkinter.LEFT)
    synchSingle.pack(side=Tkinter.RIGHT)

    global status, progress
    row = Tkinter.Frame(form)
    lab = Tkinter.Label(row, width=15, anchor='w', textvariable=status)
    pb = ttk.Progressbar(row, orient=Tkinter.HORIZONTAL, length=200, mode='determinate',
                         value=0, variable=progress, maximum=100)
    row.grid(row=11, column=1, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)
    lab.pack(side=Tkinter.LEFT)
    pb.pack(side=Tkinter.RIGHT, expand=Tkinter.YES, fill=Tkinter.X)

    row = Tkinter.Frame(form)
    ad = Tkinter.Button(row, text='admin', command=admin)
    rd = Tkinter.Button(row, text='einlesen', command=lambda: read())
    rn = Tkinter.Button(row, text='erstellen', command=lambda: run(pb, rn))
    exit = Tkinter.Button(row, text='beenden', command=bye)
    row.grid(row=11, column=2, columnspan=1, rowspan=1, sticky='NS', padx=5, pady=5)
    ad.pack(side=Tkinter.LEFT)
    rd.pack(side=Tkinter.LEFT)
    rn.pack(side=Tkinter.LEFT)
    exit.pack(side=Tkinter.RIGHT)

# ------------------------------


def terminate_threads():
    global aboutToExit
    aboutToExit.set()
    while len(threading.enumerate()) > 1:
        time.sleep(0.5)
    aboutToExit.clear()

# ------------------------------


def bye():
    logging.debug("")
    logging.info("_"*30)
    logging.info("bye!")
    global root
    if watchThread is not None and watchThread.isAlive():
        logging.info("terminating threads...")
        terminate_threads()
    root.quit()

# ------------------------------


def keyPressEvent(event):
    global root
    if event.keysym == 'Escape':
        bye()

# ------------------------------


def center(toplevel):
    toplevel.update_idletasks()
    w = toplevel.winfo_screenwidth()
    h = toplevel.winfo_screenheight()
    size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
    x = w/2 - size[0]/2
    y = h/2 - size[1]/2
    toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))

# ------------------------------


def place_side(toplevel):
    toplevel.update_idletasks()
    w = toplevel.winfo_screenwidth()
    h = toplevel.winfo_screenheight()
    size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
    global rootSize
    x = w/2 + rootSize[0]/2
    y = h/2 - size[1]/2
    toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))

# ------------------------------


def main():
    global root
    root = Tkinter.Tk()

    global progress, material, tool, status, statusNabe, statusRuecken
    progress = Tkinter.IntVar()
    progress.set(0)
    material = Tkinter.StringVar()
    material.set("Alu")
    tool = Tkinter.StringVar()
    tool.set("3R+1R")
    status = Tkinter.StringVar()
    status.set("bereit")
    statusNabe = Tkinter.StringVar()
    statusNabe.set("bereit")
    statusRuecken = Tkinter.StringVar()
    statusRuecken.set("bereit")

    root.wm_title('Erstellung der Auswuchttabellen')
    gui(root)
    root.resizable(0, 0)
    center(root)
    global rootSize
    rootSize = tuple(int(_) for _ in root.geometry().split('+')[0].split('x'))

    root.bind_all('<KeyPress>', keyPressEvent)
    root.protocol('WM_DELETE_WINDOW', bye)

    commonEntries["Artikelnummer"].insert(0, "None")

    # rueckenEntries["Durchmesser"].insert(0, "4.745")
    # rueckenEntries["Fraestiefe"].insert(0, "1.0")
    # rueckenEntries["Fraeslaenge"].insert(0, "0.9")
    # rueckenEntries["1. Entnahme"].insert(0, "140.0")
    # rueckenEntries["2. Entnahme"].insert(0, "70.0")

    # nabeEntries["Durchmesser"].insert(0, "4.745")
    # nabeEntries["Fraestiefe"].insert(0, "1.0")
    # nabeEntries["Fraeslaenge"].insert(0, "0.9")
    # nabeEntries["1. Entnahme"].insert(0, "140.0")
    # nabeEntries["2. Entnahme"].insert(0, "70.0")
    # nabeEntries["Nabenhoehe"].insert(0, "6.0")

    root.mainloop()

# ------------------------------

if __name__ == '__main__':
    main()