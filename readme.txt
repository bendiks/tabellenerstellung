'''
author: Dr. Vladimir Molchanov
date: 23.07.2015
'''

Die Daten werden im Ordner Tabellen/3R und im Ordner Tabellen/3R+1R f�r das alte bzw. das neue Werkzeug generiert. 
Keine Unterordner werden bei der Synchronisierung unterst�tzt.

Bei Fehler:
schicken Sie die Log-Datei auswuchttabellenerstellung.log 
zusammen mit einem Bildschirmfoto (oder Information �ber die eingegebenen Parameter und die Fehlermeldung in Textform) 
dem Entwickler (Bendiks Engineering GmbH & Co. KG) zu.